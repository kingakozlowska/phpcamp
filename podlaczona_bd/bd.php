<?php

interface iDB {
    /**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName);
    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query);
    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows();
    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getRow();
    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows();
}

class DB implements iDB
{
    private $link;
    private $queryResult;
    /**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName)
    {
        $this->link = mysqli_connect($host, $login, $password, $dbName);
        if (!$this->link) {
            throw new Exception ('Nie udalo sie polaczyc z baza danych: ' . $dbName);
        }
    }

    /** Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query)
    {
        $this->queryResult = mysqli_query($this->link, $query);

        return (bool)$this->queryResult;
    }

    /*** Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }

    /** Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
    @return mixed
     */
    public function getRow()
    {
        if (!$this->queryResult) {
            throw new Exception('Zapytanie nie zostalo wykonane');
        }

        return mysqli_fetch_assoc($this->queryResult);
    }

    /*** Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows()
    {
        $results = array();

        while (($row = $this->getRow())) {
            $results[] = $row;
        }

        return $results;
    }
}
echo "klasa dziala </br></br>";

?>
<!-- forlularz  dodawania do bazy-->
Dodawanie rekordu do bazy danych:
</br>
<form name="formularz1" method="get" action="dodaj_do_bd.php">
    <div class="form-group">
        <label for="name_f">Name</label> <!-- name_f -> name in form -->
        <input type="text" class="form-control" name="name_f" onblur="" value="Jan">
    </div>
    <div class="form-group">
        <label for="surname_f">Surname</label>
        <input type="text" class="form-control" name="surname_f" onblur="" value="Nowak">
    </div>
    <div class="form-group">
        <label for="gender_f">Gender</label>
        <input type="text" class="form-control" name="gender_f" onblur="" value="male">
    </div>
    <div class="form-group">
        <label for="date_of_birth_f">Date of birth</label>
        <input type="text" class="form-control" name="date_of_birth_f" onblur="" value="1990-08-01">
    </div>
    <div class="form-group">
        <label for="orders_count_f">Orders count</label>
        <input type="text" class="form-control" name="orders_count_f" onblur="" value="1000">
    </div>
    <div class="form-group">
        <label for="street_f">Street</label>
        <input type="text" class="form-control" name="street_f" onblur="" value="Cyfrowa">
    </div>
    <div class="form-group">
        <label for="city_f">City</label>
        <input type="text" class="form-control" name="city_f" onblur="" value="1">
    </div>
    <div class="form-group">
        <label for="post_code_f">Post code</label>
        <input type="text" class="form-control" name="post_code_f" onblur="" value="12111">
    </div>
    <div class="form-group">
        <label for="country_f">Country</label>
        <input type="text" class="form-control" name="country_f" onblur="" value="1">
    </div>
    <div class="form-group">
        <label for="notes_f">Notes</label>
        <textarea class="form-control" name="notes_f" onblur="">1</textarea>
    </div>

    <input type="submit" value="Dodaj" >

</form>


<!-- forlularz  edycji w bd-->
Edycja rekordu w bazie danych:
</br>
<form name="formularz1" method="get" action="edytuj_rekord_w_bd.php">
    <div class="form-group">
        <label for="id_edit_f">ID rekordu do edycji: </label> <!-- name_f -> name in form -->
        <input type="text" class="form-control" name="id_edit_f" onblur="" value="980000">
    </div>
    <input type="submit" value="Edytuj" >
</form>

<!-- forlularz  usuwania rekordu w bd-->
Usuwanie rekordu w bazie danych:
</br>
<form name="formularz1" method="get" action="usun_rekord_w_bd.php">
    <div class="form-group">
        <label for="id_delete_f">ID rekordu do edycji: </label> <!-- name_f -> name in form -->
        <input type="text" class="form-control" name="id_delete_f" onblur="" value="980006">
    </div>
    <input type="submit" value="Usun" >
</form>

<?php

/*
// dodawanie do bazy
$name = $_GET['name_f'];
$surname = $_GET['surname_f'];
$gender = $_GET['gender_f'];
$date_of_birth = $_GET['date_of_birth_f'];//." 00:00:00";
$orders_count = $_GET['orders_count_f'];
$street = $_GET['street_f'];
$city = $_GET['city_f'];
$post_code = $_GET['post_code_f'];
$country = $_GET['country_f'];
$notes = $_GET['notes_f'];


echo $name." ".$surname." ".$gender." ".$date_of_birth." ".$orders_count." ".$street." ".$city." ".$post_code." ".$country." ".$notes."</br>";


$DB = new DB('localhost','root','root','phpcamp_kkozlowska');
echo "</br>baza podlaczona</br></br>";


$DB->query("INSERT INTO `clients` (`id`, `name`, `surname`, `gender`, `date_of_birth`, `orders_count`, `street`, `city`, `postcode`, `country`, `notes`) VALUES (NULL, '$name', '$surname', '$gender', '$date_of_birth', '$orders_count', '$street', '$city', '$post_code', '$country', '$notes')");



/*
//$DB->query('INSERT INTO `clients`(`name`, `surname`, `gender`, `date_of_birth`, `orders_count`, `street`, `city`, `post_code`, `country`, `notes`) VALUES ('$name', '$surname', '$gender', '$date_of_birth', '$orders_count', '$street', '$city', '$post_code', '$country', '$notes') ');
//$a = "INSERT INTO `clients`(`name`, `surname`, `gender`, `date_of_birth`, `orders_count`, `street`, `city`, `post_code`, `country`, `notes`) VALUES ('$name', '$surname', '$gender', '$date_of_birth', '$orders_count', '$street', '$city', '$post_code', '$country', '$notes')";
//echo $a;


echo $DB->query('SELECT `gender`, COUNT(*) FROM `clients` GROUP BY `gender`');

//echo "$a</br>";
//echo 'zapytanie ok';
/*

$DB->query('INSERT INTO `clients`(`name`, `surname`, `gender`, `date_of_birth`, `orders_count`, `street`, `city`, `post_code`, `country`, `notes`)
              VALUES ('$name', '$surname', '$gender', '$date_of_birth', '$orders_count', '$street', '$city', '$post_code', '$country', '$notes')');

echo "zapytanie dziala </br>";


// wyjmowanie z bazy

$imie_edit = null;

$all = $DB->query('SELECT `name`, `surname`, `gender`, `date_of_birth`, `orders_count`, `street`, `city`, `post_code`, `country`, `notes` FROM `clients` WHEN `id`="$imie_edit"');
$first_row = $DB->getRow();
*/
?>






