<?php

class BazaProduct{
/*
    protected $id;
    protected $name;
    protected $price;
    protected $currency; //waluta
    protected $description;
    protected $picture;
    protected $producer;
    protected $category;
*/

    //__set - próba zapisania wartości do składowej, która nie istnieje lub jest niewidoczna dla klasy
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    //__get - próba dostępu do składowej, która nie istnieje lub jest niewidoczna dla klasy
    public function __get($name)
    {
        return $this->$name;
    }

    //__call - wywołanie nieistniejącej metody $name - metoda $arguments -  jako tablica od 0,
    public function __call($name, $arguments)
    {
        // jesli pierwsze 3 litery to set to.. // i czy 3 litera czy jest duza
        if((preg_match('/^set.*$/', $name)) && (ctype_upper($name[3]))){
            $funkcja = strtolower(substr($name, 3));
            //echo $funkcja;
            $this->$funkcja = $arguments[0];
        }
        elseif(preg_match('/^get.*$/', $name) && (ctype_upper($name[3]))){
            $funkcja = strtolower(substr($name, 3));
            return $this->$funkcja;
        }
    }


}



?>