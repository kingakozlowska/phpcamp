<?php

interface iDB {
    /**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName);
    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query);
    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows();
    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getRow();
    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows();
}

class DB implements iDB
{
    private $link;
    private $queryResult;
    /**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName)
    {
        $this->link = mysqli_connect($host, $login, $password, $dbName);
        if (!$this->link) {
            throw new Exception ('Nie udalo sie polaczyc z baza danych: ' . $dbName);
        }
    }

    /** Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query)
    {
        $this->queryResult = mysqli_query($this->link, $query);

        return (bool)$this->queryResult;
    }

    /*** Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }

    /** Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
    @return mixed
     */
    public function getRow()
    {
        if (!$this->queryResult) {
            throw new Exception('Zapytanie nie zostalo wykonane');
        }

        return mysqli_fetch_assoc($this->queryResult);
    }

    /*** Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows()
    {
        $results = array();

        while (($row = $this->getRow())) {
            $results[] = $row;
        }

        return $results;
    }
/*
    public function checkProduct($name_product, $name_bd){
        echo "123";
        $ilosc_produktow_w_bazie = $name_bd->query('SELECT `id`, COUNT(*) FROM `clients`');

        for($i=1;$i<=$ilosc_produktow_w_bazie;$i++) {

            $sprawdzany_produkt = $name_bd->query('SELECT `name` FROM `clients` WHERE `id`=$i');

            if ($name_product == $sprawdzany_produkt ){
                echo "Taki produkt istnieje";
            }
        else {
            echo "Produktu nie istnieje";
        }
        }
    }
*/
    public function addProduct(){

    }

    public function removeProduct(){

    }


}

?>
<!--
</br>
<form name="formularz1" method="get">
    <div class="form-group">
        <label for="name_f">Name</label> <!-- name_f -> name in form
        <input type="text" class="form-control" name="name_f" onblur="" value="produkt1">
    </div>
    <div class="form-group">
        <label for="price_f">Price</label>
        <input type="text" class="form-control" name="price_f" onblur="" value="111">
    </div>
    <div class="form-group">
        <label for="function_f">Function</label>
        <input type="text" class="form-control" name="function_f" onblur="" value="checkProduct">
    </div>
    <div class="form-group">
        <label for="id_f">ID</label>
        <input type="text" class="form-control" name="id_f" onblur="">
    </div>


    <input type="submit" value="Dodaj">

</form> -->

<?php
/*
$name = $_GET['name_f'];
$price = $_GET['price_f'];
$function = $_GET['function_f'];
$id = $_GET['id_f'];

echo $name;
echo $price;
echo $function;
echo $id;

$DB = new DB('localhost','root','root','phpcamp_kkozlowska'); */     //echo $DB->checkProduct($name, $DB);

$products = array(
    1 => array('id'=> 1, 'name' => 'towar1', 'cena' => '99 zl'),
    2 => array('id'=> 2, 'name' => 'towar2', 'cena' => '99 zl'),
    3 => array('id'=> 3, 'name' => 'towar3', 'cena' => '99 zl'),
    4 => array('id'=> 4, 'name' => 'towar4', 'cena' => '99 zl'),
    5 => array('id'=> 5, 'name' => 'towar5', 'cena' => '99 zl'),
    6 => array('id'=> 6, 'name' => 'towar6', 'cena' => '99 zl'),
    7 => array('id'=> 7, 'name' => 'towar7', 'cena' => '99 zl'),
    8 => array('id'=> 8, 'name' => 'towar8', 'cena' => '99 zl'),
    9 => array('id'=> 9, 'name' => 'towar9', 'cena' => '99 zl')
);

/*
$name = $_GET['nazwa']."</br>";
$action = $_GET['action']."</br>";
$price = $_GET['price']."</br>";

//echo $action, $name, $price;
/*
switch($_GET['action']){
    case 'checkProduct':
        if($products[$_GET['product']]){
            echo 'Jest towar'.'</br>';
        }
        break;
    case 'addProduct':
        $products[$_GET['product']] = array('price' => $_GET['price'],'nazwa'=>$_GET['nazwa']);
        break;
    case 'removeProduct':
        unset($products[$_GET['product']]);
        break;

}*/

class ServiceFunction
{
    public function checkProduct($products){
        if($products[$_GET['product']]){
            echo 'Jest towar'.'</br>';
        }
    }

    public function addProduct($products){
        $products[$_GET['product']] = array('price' => $_GET['price'], 'nazwa'=>$_GET['nazwa']); //('price' =>  $name , 'nazwa'=> $price);
        //echo 'dodano produkt';
        return 'dodano_produkt';
    }

    public function removeProduct($products){
        unset($products[$_GET['product']]);
        echo 'usunieto produkt';
    }

  }

    $options = array('uri' => 'http://localhost/dzien2');
    $server = new SoapServer(NULL, $options);
    $server->setClass('ServiceFunction');
    $server->handle();



















// JSON

//var_dump($products);

//$new_json = json_decode($products);
//var_dump($new_json);
//echo $products;

//json_decode();


// XML
/*
header("Content-type, text/xml");
$list = array('jajka', 'boczek', 'cebula');
$simplexml = new SimpleXmlElement('<?xml version="1.0"?><list />');

foreach ($list as $item){
    $simplexml->addChild('item', $item);
}

echo $simplexml->asXML();

*/




//var_dump($products);





// curl
//$DB = new DB('localhost','root','root','phpcamp_kkozlowska');
//echo "</br>baza podlaczona</br>";
//$DB->query('INSERT INTO `clients`(`name`, `surname`, `gender`, `date_of_birth`, `orders_count`, `street`, `city`, `post_code`, `country`, `notes`) VALUES ('$name', '$surname', '$gender', '$date_of_birth', '$orders_count', '$street', '$city', '$post_code', '$country', '$notes') ');





?>