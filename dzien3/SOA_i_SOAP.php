/*

SOA czym jest?
 - Z perspektywy IT widzimy infrastrukturę potrzebną do dostarczenia Usług wspierających realizację procesów biznesowych.
Cele i parametry procesu biznesowego są wyznacznikiem, do którego muszą być dopasowane komponenty SOA, zapewniając
wymagany poziom dostarczanej usługi

Mianem usługi określa się tu każdy element oprogramowania, mogący działać niezależnie
od innych oraz posiadający zdefiniowany interfejs, za pomocą którego udostępnia realizowane funkcje. Sposób działania każdej usługi
jest w całości zdefiniowany przez interfejs ukrywający szczegóły implementacyjne - niewidoczne i nieistotne z punktu widzenia klientów.



*/